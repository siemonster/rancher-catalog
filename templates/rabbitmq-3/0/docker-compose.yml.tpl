version: '2'
services:
  rabbitmq:
    image: registry.gitlab.com/siemonster/rabbitmq-conf:siemonster
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name=$${stack_name}/$${service_name}
      io.rancher.sidekicks: rabbitmq-base,rabbitmq-datavolume
      io.rancher.container.pull_image: always
    volumes_from:
      - rabbitmq-datavolume
    environment:
      - RABBITMQ_HIPE_COMPILE=${RABBITMQ_HIPE_COMPILE}
      - RABBITMQ_NET_TICKTIME=60
      - RABBITMQ_CLUSTER_PARTITION_HANDLING=autoheal
      - CONFD_ARGS=-interval 5 -watch
      - RABBITMQ_USER=${RABBITMQ_USER}
      - RABBITMQ_PASSWORD=${RABBITMQ_PASSWORD}
  rabbitmq-datavolume:
    network_mode: "none"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name=$${stack_name}/$${service_name}
      io.rancher.container.start_once: true
      io.rancher.container.pull_image: always
    volumes:
      - rabbitconf:/etc/rabbitmq
      - rancherbin:/opt/rancher/bin
    entrypoint: /bin/true
    image: registry.gitlab.com/siemonster/rabbitmq:master
  rabbitmq-base:
    network_mode: "container:rabbitmq"
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.scheduler.affinity:container_label_soft_ne: io.rancher.stack_service.name=$${stack_name}/$${service_name}
      io.rancher.container.pull_image: always
    image: registry.gitlab.com/siemonster/rabbitmq:master
    restart: always
    volumes_from:
      - rabbitmq-datavolume
    entrypoint:
      - /opt/rancher/bin/run.sh
    environment:
      - RABBITMQ_ERLANG_COOKIE=${erlang_cookie}
volumes:
  rancherbin:
    driver: local
    per_container: true
  rabbitconf:
    driver: local
    per_container: true
